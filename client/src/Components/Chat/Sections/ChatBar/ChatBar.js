import React, { Component } from 'react'
import { Send } from '@material-ui/icons';

import "./ChatBar.scss"

export default class ContactBar extends Component {

    messager = (e) => {

        e.preventDefault();

        var today = new Date()

        var msg = document.getElementById('msgText').value;
        if (!msg.trim() == "") {
            document.getElementById('msgText').value = "";

            var scrollable = document.getElementById('chatterBox')
            scrollable.innerHTML += `
                <div class="message sent">
                    ${msg}
                                <span class="metadata">
                        <span class="time">${today.getHours()}:${today.getMinutes()}</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                    </span>
                </div>
                <div class="message received">
                    ${msg.length}
                    <span class="metadata" ><span class="time">${today.getHours()}:${today.getMinutes()}</span></span>
                </div>
            `
            scrollable.scrollTop = scrollable.scrollHeight
        }

    }

    componentDidMount() {
        var scrollable = document.getElementById('chatterBox')
        scrollable.scrollTop = scrollable.scrollHeight
    }


    render() {
        return (
            <React.Fragment>
                <div className="title" >
                    <div className="avatar" >
                        <h3>TE-A</h3>
                    </div>
                    <div className="class_name" >
                        <h3>TE-A</h3>
                    </div>
                </div>
                <div id="chatterOuterBox" style={{ height: window.innerHeight - 220 }} >
                    <div id="chatterBox" >
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                            Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                            Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.Halo frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                        <div style={{width:"100%",position:"relative",display:"flex",alignItems:"center"}} >
                            <div className="circleSender" >
                                PB
                            </div>
                            <div class="message received">
                                Halo Frands.
                            <span class="metadata" ><span class="time">9:03 pm</span></span>
                            </div>
                        </div>
                        <div class="message sent">
                            Chai pilo.
                            <span class="metadata">
                                <span class="time">9:04 pm</span><span class="tick"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="15" id="msg-dblcheck-ack" x="2063" y="2076"><path d="M15.01 3.316l-.478-.372a.365.365 0 0 0-.51.063L8.666 9.88a.32.32 0 0 1-.484.032l-.358-.325a.32.32 0 0 0-.484.032l-.378.48a.418.418 0 0 0 .036.54l1.32 1.267a.32.32 0 0 0 .484-.034l6.272-8.048a.366.366 0 0 0-.064-.512zm-4.1 0l-.478-.372a.365.365 0 0 0-.51.063L4.566 9.88a.32.32 0 0 1-.484.032L1.892 7.77a.366.366 0 0 0-.516.005l-.423.433a.364.364 0 0 0 .006.514l3.255 3.185a.32.32 0 0 0 .484-.033l6.272-8.048a.365.365 0 0 0-.063-.51z" fill="#4fc3f7" /></svg></span>
                            </span>
                        </div>
                    </div>
                </div>
                <form className="typer" onSubmit={(e) => { this.messager(e) }} >
                    <div className="inputDiv" >
                        <input id="msgText" type="text" placeholder="Type a message" autoFocus />
                    </div>
                    <button class="send" onClick={(e) => { this.messager(e) }} >
                        <div class="circle">
                            <Send style={{ marginLeft: 5 }} />
                        </div>
                    </button>
                </form>
            </React.Fragment>
        )
    }
}
